[## About

This is a helper module when you want to check the source of a migrated Drupal entity on the UI.
Using this there is no need to set a custom field to track the old id.

Provides a simple Block with a link to the old Entity of a migrated Entity.
For example: {OLD_DRUPAL_URL}/node/{sourceid1}.
Behind the scenes it uses the migration tables to get the sourceid1 with a simple database query.
It is something like a reverse migrate_lookup.

## Usage

- Enable the module after you have finished some migrations. The core `migrate` module is required.
- Add a new Block instance of type "Migrate Sourceid" on the Block Layout (`/admin/structure/block`).
- You can enable the Block for any entity that has a view page. Eg `/node/*, /taxonomy/term/*` etc).
- Export config to get the settings yml in the files (an alternative is to override it through settings.php).
- Override the `migrate_sourceid.settings.yml` accordingly.
- For each entity type you can add the migration ids you want to look for.
- Import the new `migrate_sourceid.settings.yml` file.
- Clear caches.

## Example of configuration settings

```yml
source_url: https://www.example.com
migrations:
  node:
    - d7_node
    - my_custom_csv_migration
    - node_news
    - node_page
    - node_article
  block_content:
    - d7_block_content
  file:
    - d7_file
  media:
    - my_media_file
  taxonomy_term:
    - d7_taxonomy_term
  user:
    - d6_user
```

## ToDo

- Add permissions
- Add a Drupal form to override settings
- Allow to override link texts and prefix
- Create a Drupal service
]()
