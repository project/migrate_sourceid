<?php

namespace Drupal\migrate_sourceid\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Schema;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A simple block that contains a link to open the old entity migrated URL.
 *
 * @Block(
 *   id = "migrate_sourceid",
 *   admin_label = @Translation("Migrate Sourceid"),
 *   category = @Translation("migrate")
 * )
 */
final class MigrateSourceidBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The current_route_match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRouteMatch;

  /**
   * The config_factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The string_translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * The Drupal database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private Connection $connection;

  /**
   * The Drupal database schema.
   *
   * @var \Drupal\Core\Database\Schema
   */
  private Schema $schema;

  /**
   * Constructs a new TaxonomyOpenLinkBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config_factory service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current_route_match service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $current_string_translation
   *   The string_translation service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The drupal database connection.
   * @param \Drupal\Core\Database\Schema $schema
   *   The drupal database schema.
   */
  public function __construct(array $configuration,
                              string $plugin_id,
                              array $plugin_definition,
                              ConfigFactoryInterface $config_factory,
                              CurrentRouteMatch $current_route_match,
                              TranslationInterface $current_string_translation,
                              Connection $connection,
                              Schema $schema,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentRouteMatch = $current_route_match;
    $this->stringTranslation = $current_string_translation;
    $this->connection = $connection;
    $this->schema = $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('string_translation'),
      $container->get('database'),
      $container->get('database')->schema(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $old_site_url = $this->configFactory->get('migrate_sourceid.settings')
      ->get('source_url');
    $migrations = $this->configFactory->get('migrate_sourceid.settings')
      ->get('migrations');
    $entity_type = "";
    $entity_path_prefix = "";
    $url_markup = "";

    if (!$migrations) {
      $build['content'] = [
          '#markup' => $this->t("No migration mappings found on settings.php."),
      ];
      return $build;
    }

    if (!$old_site_url) {
      $build['content'] = [
          '#markup' => $this->t("Value for source_url not found on settings.php."),
      ];
      return $build;
    }

    // Get current page route name.
    $route_name = $this->currentRouteMatch->getRouteName();

    if ((substr($route_name, 0, 6) !== "entity")) {
      $build['content'] = [
          '#markup' => "",
      ];
      return $build;
    }

    if ($route_name === "entity.node.canonical") {
      $entity_type = "node";
      $entity_path_prefix = "node";
    }

    if ($route_name === "entity.taxonomy_term.canonical") {
      $entity_type = "taxonomy_term";
      $entity_path_prefix = "taxonomy/term";
    }

    if ($route_name === "entity.media.canonical") {
      $entity_type = "media";
      $entity_path_prefix = "media";
    }

    if ($route_name === "entity.file.canonical") {
      $entity_type = "file";
      $entity_path_prefix = "file";
    }

    if ($route_name === "entity.block_content.canonical") {
      $entity_type = "block_content";
      $entity_path_prefix = "block";
    }

    if ($route_name === "entity.user.canonical") {
      $entity_type = "user";
      $entity_path_prefix = "user";
    }

    if ($entity_type && $migrations[$entity_type]) {
      $entity = $this->currentRouteMatch->getParameter($entity_type);
      $entity_id = $entity->id();

      foreach ($migrations[$entity_type] as $migration_id) {
        $source_ids = $this->getEntitySourceidFromMigrationDestination($migration_id, $entity_id);

        // If we found some ids continue.
        if ($source_ids) {
          foreach ($source_ids as $source_id) {
            $preview_url_source = $old_site_url . "/" . $entity_path_prefix . "/" . $source_id;
            $preview_url = Url::fromUri($preview_url_source)->toString();
            $migration_title = $this->t("Migration @migration_id:", ['@migration_id' => $migration_id]);
            $url_title = $this->t("View old entity (@id)", ['@id' => $source_id]);
            $url_markup .= $migration_title . " <a href='" . $preview_url . "' target='_blank' class='button button--large button--secondary' rel='nofollow'>" . $url_title . "</a>";
          }
        }
      }
    }
    else {
      $url_markup = $this->t('No mappings found.');
    }

    $markup = "<br><div class='text-align-right'>";
    $markup .= $url_markup;
    $markup .= "</div><br>";

    $build['content'] = [
      '#markup' => $markup,
    ];
    return $build;
  }

  /**
   * Look into a Drupal migration table if there is a mapping.
   */
  public function getEntitySourceidFromMigrationDestination(string $migration_id, int $destid1): array {
    $results = [];
    $fields = ["sourceid1"];
    $table = "migrate_map_" . $migration_id;
    $table_exists = $this->schema->tableExists($table);

    if (!$table_exists) {
      return $results;
    }

    $query = $this->connection->select($table, "t")->condition("t.destid1", $destid1, "=");
    $query->fields("t", $fields);
    $records = $query->execute()->fetchAllAssoc("sourceid1");

    if ($records) {
      foreach ($records as $record) {
        foreach ($fields as $field) {
          $results[] = $record->{$field};
        }
      }
    }

    return $results;
  }

}
